# HTML 5 - Curso em Video

### Símbolos especiais

&lt; &nbsp; &gt; &nbsp; &le; &nbsp; &ge; &nbsp; &pound; &nbsp; &yen; &nbsp; &euro; &nbsp; &copy; &nbsp; &reg; 
&nbsp; &trade; &nbsp; &permil; &nbsp; &sum; &nbsp; &infin; &nbsp; &times; &nbsp; &plusmn; &nbsp; &oplus; &nbsp;
&radic; &nbsp; &ne; &nbsp; &delta; &nbsp; &lambda; &nbsp; &omega; &nbsp; &phi; &nbsp; &larr; &nbsp; &rarr; &nbsp; &uarr; &nbsp; &darr; &nbsp; &harr; &nbsp; &spades; &nbsp; &clubs; &nbsp; &hearts; &nbsp; &diams;

### Mapa de imagens

Uma cordenada possui dois números, o X e o Y. ***Ex:*** (111,111)

***Retângulo =>*** Quando o mapa for retângulo a primeiras cordenada é do canto superior esquerdo, e a segunda do
do canto inferior direito. Ex: (111,222,333,444)

***Círculo =>*** Primeiro é a cordenada do centro do círculo, depois o tamanho do raio. Ex: (111,222,12)

***Poligonal =>*** Precisa pegar a cordenada de todos pontos, em sentido horário. Uma área poligonal pode ter vários pontos. Ex com 4
pontos: (111,222,111,222,111,222,111,222)

